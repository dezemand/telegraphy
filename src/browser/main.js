"use strict";

import React from 'react';
import ReactDOM from 'react-dom';
import {MainWindow} from '../dist/components/MainWindow';

ReactDOM.render(<MainWindow/>, document.getElementById('app-mount'));
