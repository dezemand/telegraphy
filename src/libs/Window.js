"use strict"

import {BrowserWindow} from 'electron'
let self

export class Window {
  constructor(path, opts) {
    self = this
    self.opts = opts
    self.url = `file://${path}`
    self.window = null
    if(!self.opts.title) self.opts.title = 'Telegraphy'
  }
  open() {
    self.window = new BrowserWindow(self.opts)
    self.window.loadURL(self.url)
  }
}
