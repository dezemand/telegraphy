"use strict"

import {app, BrowserWindow} from 'electron'
import {MainWindow} from './MainWindow'
import path from 'path'

let basedir, mainWindow
module.exports = (dir) => basedir = dir

app.on('ready', () => {
  mainWindow = new MainWindow(basedir)
  mainWindow.open()
})

app.on('window-all-closed', () => {
  app.quit()
})
