"use strict"

module.exports = {
  ipc: {
    ACCOUNTS_LIST_REQUEST: 'ACCOUNTS_LIST_REQUEST',
    ACCOUNTS_LIST_FULL: 'ACCOUNTS_LIST_FULL'
  }
}
