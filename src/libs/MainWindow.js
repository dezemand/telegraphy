"use strict";

import { Window } from './Window';
import { ipcMain } from 'electron';
import path from 'path';
import { AccountManager } from './AccountManager';
let self;

export class MainWindow extends Window {
  constructor(basedir) {
    let options = {
      width: 1000,
      height: 600,
      frame: false
    };
    let file = path.join(basedir, 'html/main.html');
    super(file, options);
    self = this;
    self.accountManager = new AccountManager();
  }
}
